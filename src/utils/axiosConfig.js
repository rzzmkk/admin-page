import axios from "axios";
// import { history } from "../index";

const jwt = window.localStorage.getItem("authToken");

export const axiosConfig = axios.create({
  baseURL: "http://46.101.175.38:8080",
  headers: {
    "Content-Type": "application/json; charset=utf-8",
    Autorization: `Bearer ${jwt}`,
    Accept: "*/*"
  },
  timeout: 60 * 1000
});

axiosConfig.defaults.withCredentials = true;
// axiosConfig.interceptors.response.use(
//   response => response,
//   error => {
//     console.log(error, "err");
//     window.localStorage.removeItem("authToken");
//     history.push("/");
//   }
// );
