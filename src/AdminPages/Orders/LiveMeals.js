import React, { useEffect, useState } from "react";
import {
  OrdersWrapper,
  OrderWrapper,
  AddressWrapper,
  P,
  Button,
  Price,
  MealName
} from "./styles";
import { axiosConfig } from "../../utils/axiosConfig";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

async function handleClick(e, orderId) {
  await axiosConfig
    .put("/api/orders/status", null, {
      params: {
        status: 1,
        id: orderId
      }
    })
    .then(res => {
      if (res.status === 200) {
        toast("Order status has been changed", {
          autoClose: 6000,
          position: "top-right"
        });
      }
    });
}

function formatDate(date) {
  const dat = new Date(date + "");
  return (
    dat.getDate() +
    "/" +
    (dat.getMonth() + 1) +
    "/" +
    dat.getFullYear() +
    " " +
    dat.getHours() +
    ":" +
    dat.getMinutes() +
    ":" +
    dat.getSeconds()
  );
}

function LiveMeals() {
  const [orders, setOrders] = useState([]);
  const loadOrders = async () => {
    await axiosConfig.get("/api/orders").then(res => {
      if (res.status === 200) {
        setOrders(() => res.data.slice(0, 20));
      }
    });
  };
  useEffect(() => {
    loadOrders();
  }, [orders.length]);
  return (
    <React.Fragment>
      <h1>Orders page</h1>
      <OrdersWrapper>
        {orders.map(order => (
          <OrderWrapper key={order.id}>
            <AddressWrapper>
              <h4>Order info</h4>
              {order.meals.map(meal => (
                <React.Fragment key={meal.id}>
                  <MealName>
                    {meal.title} - {meal.quantity} pcs
                  </MealName>
                </React.Fragment>
              ))}
            </AddressWrapper>
            <AddressWrapper>
              <h4>User info</h4>
              <P>address:{order.user.address}</P>
              <P>
                name: {order.user.firstName} {order.user.lastName}
              </P>
              <P>email: {order.user.login}</P>
              <P>phonenumber:{order.user.phoneNumber.split("-").join("")}</P>
            </AddressWrapper>

            <AddressWrapper>
              <Price>
                Total price to pay: ${order.overallPrice.toFixed(2)}
              </Price>

              <P>Payment method: {order.paymentType}</P>
              <P>
                Order Status: {order.status ? "Being made" : "Not accepted yet"}
              </P>
              <P>Order created: {formatDate(order.createdAt)}</P>
              <Button
                key={order.id}
                type="button"
                onClick={e => handleClick(e, order.id)}
              >
                Accept order
              </Button>
            </AddressWrapper>
          </OrderWrapper>
        ))}
      </OrdersWrapper>
    </React.Fragment>
  );
}

export default LiveMeals;
