import React, { Fragment } from "react";
import { ProtectedRoute } from "../Routes/ProtectedRoute";

import AppHeader from "../../Layout/AppHeader/";
import AppSidebar from "../../Layout/AppSidebar/";

import LiveMeals from "./LiveMeals";

const Orders = ({ match }) => (
  <Fragment>
    <AppHeader />
    <div className="app-main">
      <AppSidebar />
      <div className="app-main__outer">
        <div className="app-main__inner">
          <ProtectedRoute path={`${match.url}/live`} component={LiveMeals} />
          {/* <ProtectedRoute path={`${match.url}/add`} component={AddMeals} /> */}
        </div>
      </div>
    </div>
  </Fragment>
);

export default Orders;
