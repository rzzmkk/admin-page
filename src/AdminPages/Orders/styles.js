import styled from "styled-components";

export const OrdersWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column-reverse;

  align-items: center;
  justify-content: flex-start;
  box-sizing: border-box;
  margin: 0;
`;

export const OrderWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: row;
  background-color: white;
  justify-content: space-around;
  margin: 0 auto;
  align-items: flex-start;
  border-radius: 8px;
  padding-bottom: 10px;
  border: 1px solid #f48fb1;
  margin-bottom: 20px;
`;

export const AddressWrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  width: 30%;
`;

export const P = styled.p`
  margin: 0;
  padding: 0;
  font-size: 16px;
`;

export const Button = styled.button`
  width: 100%;
  background-color: #f48fb1;
  border-radius: 8px;
  color: white;
  border: 1px solid #f8869b;
`;

export const Price = styled(P)`
  color: red;
  font-size: 20px;
`;

export const MealName = styled(P)`
  font-weight: 600;
`;
