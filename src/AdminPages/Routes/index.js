import React from "react";
import { Switch, Route } from "react-router-dom";
import { ProtectedRoute } from "./ProtectedRoute";

import Login from "../Login";
import Dashboards from "../../DemoPages/Dashboards";

export const Routes = () => (
  <Switch>
    <Route exact path="/" component={Login} />
    <ProtectedRoute path="/dashboards" component={Dashboards} />
  </Switch>
);
