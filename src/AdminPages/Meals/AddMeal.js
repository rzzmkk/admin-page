import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import styled from "styled-components";
import Select from "react-select";
import { axiosConfig } from "../../utils/axiosConfig";

const Button = styled.button`
  background-color: #fa788e;
  height: 50px;
  color: white;
`;

const AddMeal = () => {
  const [message, setMessage] = useState("");
  const [categories, setCategories] = useState([]);
  const options = categories.map(x => {
    return {
      value: x.id,
      label: x.name
    };
  });
  useEffect(() => {
    async function getCategories() {
      await axiosConfig.get("/api/categories").then(res => {
        if (res.status === 200) {
          setCategories(() => res.data);
          window.localStorage.setItem("categories", JSON.stringify(res.data));
        }
      });
    }
    getCategories();
  }, []);
  return (
    <div>
      <h1>Add meal</h1>
      <Formik
        initialValues={{
          title: "",
          price: 0,
          ingredients: "",
          description: "",
          tag: "",
          url: "",
          quantity: 1,
          category: {
            id: 1,
            name: ""
          },
          isAddedToCart: false,
          isAddedBtn: false
        }}
        onSubmit={async (values, { setSubmitting }) => {
          let form = new FormData();
          form.append("file", values.url);
          await axiosConfig
            .post("/api/files/uploadFile", form, {
              headers: {
                "Content-type": "multipart/form-data"
              }
            })
            .then(res => {
              values.url = res.data.fileDownloadUri;
              if (res.status === 200) {
                axiosConfig.post("api/meals", values).then(res => {
                  if (res.status === 201) {
                    setMessage(() => "Meal successfully added");
                  }
                });
              }
            });

          setSubmitting(false);
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue
        }) => (
          <form className="meal-form" onSubmit={handleSubmit}>
            <div className="form-group">
              <label>Title</label>
              <input
                type="text"
                name="title"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.title}
              />
            </div>
            <div className="form-group">
              <label>Price</label>
              <input
                type="number"
                name="price"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.price}
              />
            </div>
            <div className="form-group">
              <label>Description</label>
              <input
                type="text"
                name="description"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.description}
              />
            </div>
            <div className="form-group">
              <label>image</label>
              <input
                type="file"
                name="url"
                id="url"
                onChange={function(event) {
                  setFieldValue("url", event.currentTarget.files[0]);
                }}
                onBlur={handleBlur}
              />
            </div>
            <div className="form-group">
              <label>Category</label>
              <Select
                options={options}
                name="category"
                onChange={selectedOption => {
                  values.category.name = selectedOption.label;
                  values.category.id = selectedOption.value;
                  console.log(values);
                }}
              />
              {/* <Answer value={"values.category"} /> */}
            </div>
            <p>{message.length !== 0 ? message : null}</p>
            <Button type="submit" disabled={isSubmitting}>
              Add Meal
            </Button>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default AddMeal;
