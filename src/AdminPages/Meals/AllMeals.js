import React, { Fragment } from "react";
import MealsTable from "./MealsTable";
import { axiosConfig } from "../../utils/axiosConfig";

export default class AllMeals extends React.Component {
  constructor(props) {
    super(props);
    this.state = { meals: [] };
  }
  async componentDidMount() {
    await axiosConfig.get("api/meals").then(res => {
      if (res.status === 200) {
        console.log(res);
        this.setState({ meals: res.data });
      }
    });
  }
  render() {
    return (
      <div className="app-main">
        <MealsTable data={this.state.meals} />
      </div>
    );
  }
}
