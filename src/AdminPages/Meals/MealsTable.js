import React, { useState, useEffect } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from "react-bootstrap-table2-editor";
import { axiosConfig } from "../../utils/axiosConfig";
import paginationFactory from "react-bootstrap-table2-paginator";
import "./table.css";
import styled from "styled-components";

import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

const Button = styled.button`
  width: 150px;
  background-color: #f8869b;
  border-radius: 8px;
  border: 1px solid #f8869b;
  box-shadow: 4px 4px 2px 0px rgba(0, 0, 0, 0.75);
`;

const columns = [
  {
    dataField: "id",
    text: "ID",
    sort: true
  },
  {
    dataField: "title",
    text: "Name",
    sort: true
  },
  {
    dataField: "price",
    text: "Price",
    sort: true
  },
  {
    dataField: "description",
    text: "Description"
  },
  {
    dataField: "category.name",
    text: "Category"
  }
];
const defaultSorted = [
  {
    dataField: "id", // if dataField is not match to any column you defined, it will be ignored.
    order: "asc" // desc or asc
  }
];
const afterSaveCell = async (column, oldValue, row, newValue) => {
  const body = JSON.stringify(row);
  await axiosConfig.put("/api/meals", body).then(res => {
    if (res.status === 200) {
      toast("Meal has been changed", {
        autoClose: 6000,
        position: "top-right"
      });
    }
  });
};

function MealsTable(props) {
  const [selectedRow, setSelectedRow] = useState({});
  useEffect(() => {
    console.log(selectedRow);
  }, [selectedRow]);

  const CaptionElement = () => (
    <React.Fragment>
      <h3
        style={{
          borderRadius: "0.25em",
          textAlign: "center",
          color: "purple",
          border: "1px solid purple",
          padding: "0.5em"
        }}
      >
        Meals
      </h3>
      <Button type="button" onClick={handleClick}>
        delete
      </Button>
    </React.Fragment>
  );

  const selectRow = {
    mode: "radio",
    bgColor: "#00BFFF",
    clickToSelect: true,
    clickToEdit: true,
    blurToSaveCell: true,
    onSelect: (row, isSelect, rowIndex, e) => {
      setSelectedRow(row);
      console.log(selectedRow, "row selected");
    }
  };

  async function handleClick() {
    await axiosConfig.delete("/api/meals", { data: selectedRow }).then(res => {
      if (res.status === 200) {
        toast("Meal has been deleted", {
          autoClose: 6000,
          position: "top-right"
        });
      }
    });
  }

  return (
    <div className="table">
      <BootstrapTable
        defaultSorted={defaultSorted}
        keyField="id"
        bordered={false}
        data={props.data}
        columns={columns}
        caption={<CaptionElement />}
        selectRow={selectRow}
        pagination={paginationFactory()}
        cellEdit={cellEditFactory({
          mode: "dbclick",
          afterSaveCell,
          blurToSaveCell: true
        })}
      />
    </div>
  );
}

export default MealsTable;
