import React, { Fragment } from "react";
import { Route } from "react-router-dom";
import { ProtectedRoute } from "../Routes/ProtectedRoute";

import AppHeader from "../../Layout/AppHeader/";
import AppSidebar from "../../Layout/AppSidebar/";

import AddMeals from "./AddMeal";
import AllMeals from "./AllMeals";

const Meals = ({ match }) => (
  <Fragment>
    <AppHeader />
    <div className="app-main">
      <AppSidebar />
      <div className="app-main__outer">
        <div className="app-main__inner">
          <ProtectedRoute path={`${match.url}/all`} component={AllMeals} />
          <ProtectedRoute path={`${match.url}/add`} component={AddMeals} />
        </div>
      </div>
    </div>
  </Fragment>
);

export default Meals;
