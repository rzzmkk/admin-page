import React from "react";
import {
  LoginWrapper,
  Field,
  Input,
  Button,
  ErrorMessage,
  Form
} from "./styles";
import { Formik } from "formik";
import { axiosConfig } from "../../utils/axiosConfig";

import { loginValidationSchema } from "./ValidationSchema";

export default function Login(props) {
  return (
    <LoginWrapper>
      <h1>Login</h1>
      <Formik
        initialValues={{ email: "", password: "" }}
        onSubmit={async (values, { setSubmitting }) => {
          let body = {
            login: values.email,
            password: values.password
          };
          const res = await axiosConfig.post("/login", body);
          if (res.status === 200) {
            window.localStorage.setItem("authToken", res.data);
            props.history.push("/dashboards/basic");
          } else {
            console.log("oops.... something went wrong");
          }
          return () => {
            setSubmitting(false);
          };
        }}
        validationSchema={loginValidationSchema}
      >
        {({
          values,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          errors,
          touched
        }) => (
          <Form onSubmit={handleSubmit}>
            <Field>
              <Input
                type="text"
                name="email"
                placeholder="Email *"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.email}
              />
            </Field>
            {errors.email && touched.email ? (
              <ErrorMessage>{errors.email}</ErrorMessage>
            ) : (
              ""
            )}
            <Field>
              <Input
                type="password"
                name="password"
                placeholder="Password *"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.password}
              />
            </Field>
            {errors.password && touched.password ? (
              <ErrorMessage>{errors.password}</ErrorMessage>
            ) : (
              ""
            )}
            <Button type="submit" disabled={isSubmitting}>
              Sign In
            </Button>
          </Form>
        )}
      </Formik>
    </LoginWrapper>
  );
}
