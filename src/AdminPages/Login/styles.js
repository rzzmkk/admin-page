import styled from "styled-components";

export const LoginWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  width: 450px;
  margin: auto;
`;

export const Form = styled.form`
  width: 100%;
  height: 100%;
`;

export const Field = styled.div`
  width: 100%;
  height: 100px;
`;

export const Input = styled.input`
  width: 100%;
  height: 65px;
  padding: 10px 15px;
`;

export const Button = styled.button`
  width: 100%;
  border-radius: 8px;
  height: 60px;
  background-color: #61affe;
  cursor: pointer;

  &:disabled {
    background-color: white;
    cursor: not-allowed;
  }
`;

export const ErrorMessage = styled.span`
  margin: 0;
  padding: 0;
  width: 80%;
  text-align: center;
  color: #bb0528;
  margin-bottom: 1rem;

  &:before {
    content: "⚠️ ";
  }
`;
