import * as Yup from "yup";

export const loginValidationSchema = Yup.object().shape({
  email: Yup.string()
    .matches(
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
      {
        message: "Please enter valid email",
        excludeEmptyString: true
      }
    )
    .required("Email is required"),
  password: Yup.string()
    .min(5, "Password is too short!")
    .max(40, "Password is too long")
    .required("Password is required!")
});
