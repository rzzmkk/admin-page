import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";

import AppHeader from "../../Layout/AppHeader";
import AppSidebar from "../../Layout/AppSidebar";

import AllUsers from "./AllUsers";
import AddUser from "./AddUser";

const Users = ({ match }) => (
  <Fragment>
    <AppHeader />
    <div className="app-main">
      <AppSidebar />
      <div className="app-main__outer">
        <div className="app-main__inner">
          <Switch>
            <Route path={`${match.url}/all`} component={AllUsers} />
            <Route path={`${match.url}/add`} component={AddUser} />
          </Switch>
        </div>
      </div>
    </div>
  </Fragment>
);

export default Users;
