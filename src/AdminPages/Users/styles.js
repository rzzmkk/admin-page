import styled from "styled-components";

export const FormContainer = styled.form`
    width: 100%:
    display: flex;
    flex-direction: column;
    align-items: flex-start;
    justify-content: flex-start;
    box-sizing: border-box;

    & > div{
        width: 100%;
    }
`;
