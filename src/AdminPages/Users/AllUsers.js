import React, { Fragment, Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from "react-bootstrap-table2-editor";
import "./table.css";
import { axiosConfig } from "../../utils/axiosConfig";
class AllUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: []
    };
  }

  async componentDidMount() {
    await axiosConfig.get("/api/users").then(res => {
      if (res.status === 200) {
        this.setState({
          users: res.data
        });
      }
    });
  }
  render() {
    const selectRow = {
      mode: "checkbox",
      bgColor: "#00BFFF",
      clickToSelect: true,
      clickToEdit: true,
      blurToSaveCell: true
    };
    const { users } = this.state;
    const columns = [
      {
        dataField: "id",
        text: "ID",
        sort: true
      },
      {
        dataField: "firstName",
        text: "First name"
      },
      {
        dataField: "lastName",
        text: "Last name",
        sort: true
      },
      {
        dataField: "phoneNumber",
        text: "Phone number"
      },
      {
        dataField: "role.name",
        text: "Role"
      },
      {
        dataField: "address",
        text: "Address"
      }
    ];
    const defaultSorted = [
      {
        dataField: "id", // if dataField is not match to any column you defined, it will be ignored.
        order: "asc" // desc or asc
      }
    ];
    const CaptionElement = () => (
      <h3
        style={{
          borderRadius: "0.25em",
          textAlign: "center",
          color: "purple",
          border: "1px solid purple",
          padding: "0.5em"
        }}
      >
        Users
      </h3>
    );

    return (
      <Fragment>
        <BootstrapTable
          defaultSorted={defaultSorted}
          keyField="id"
          bordered={false}
          data={users}
          columns={columns}
          caption={<CaptionElement />}
          selectRow={selectRow}
          cellEdit={cellEditFactory({
            mode: "dbclick",
            blurToSaveCell: true
          })}
          exportCSV
        />
      </Fragment>
    );
  }
}

export default AllUsers;
