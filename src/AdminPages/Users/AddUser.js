import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import { axiosConfig } from "../../utils/axiosConfig";

import { FormContainer } from "./styles";

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
      width: 200
    }
  }
}));

// {
//     "firstName":"aa",
//     "lastName":"bb",
//     "phoneNumber":"1233212233",
//     "login" : "aaaa@asdasd.com",
//     "address" : "Gnidina 228 - 666",
//     "birthday": "1999-02-22",
//     "password":"Password1",
//     "role":{"id":3}
//   }

function AddUser() {
  const classes = useStyles();
  const [message, setMessage] = useState("");
  const [userData, setUserData] = useState({
    birthday: "2019-12-12",
    role: {
      id: 3
    }
  });

  async function handleSubmit(e) {
    e.preventDefault();
    await axiosConfig
      .post("/api/users/restaurant", userData)
      .then(res => {
        if (res.status === 200) {
          setMessage(() => "User successfully created");
        }
      })
      .catch(e => console.error(e));
    console.log(userData);
  }
  function handleChange(e) {
    const { name, value } = e.target;
    if (value.length === 0) {
      setMessage(() => "Fill all fields");
    } else if (name === "password" && value.length < 8) {
      setMessage(() => "Set password at least 8 characters");
    } else {
      userData[name] = value;
    }
  }

  return (
    <FormContainer className={classes.root} noValidate autoComplete="off">
      <TextField
        name="firstName"
        onChange={handleChange}
        label="First Name"
        variant="outlined"
      />
      <TextField
        name="lastName"
        onChange={handleChange}
        label="Last Name"
        variant="outlined"
      />
      <TextField
        name="phoneNumber"
        onChange={handleChange}
        label="Phonenumber"
        variant="outlined"
      />
      <TextField
        name="login"
        onChange={handleChange}
        label="email"
        variant="outlined"
      />
      <TextField
        name="address"
        onChange={handleChange}
        label="address"
        variant="outlined"
      />
      <TextField
        type="password"
        name="password"
        onChange={handleChange}
        label="Password"
        variant="outlined"
      />
      {message.length !== 0 ? message : null}
      <Button type="submit" onClick={handleSubmit} variant="contained">
        Create user
      </Button>
    </FormContainer>
  );
}

export default AddUser;
