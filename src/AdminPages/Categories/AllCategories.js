import React, { Fragment, useState, useEffect } from "react";
import styled from "styled-components";
import { axiosConfig } from "../../utils/axiosConfig";

import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from "react-bootstrap-table2-editor";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

const Button = styled.button`
  width: 150px;
  background-color: #f8869b;
  border-radius: 8px;
  border: 1px solid #f8869b;
  box-shadow: 4px 4px 2px 0px rgba(0, 0, 0, 0.75);
`;

const columns = [
  {
    dataField: "id",
    text: "ID",
    sort: true
  },
  {
    dataField: "name",
    text: "Name",
    sort: true
  }
];
const defaultSorted = [
  {
    dataField: "id", // if dataField is not match to any column you defined, it will be ignored.
    order: "asc" // desc or asc
  }
];
const afterSaveCell = async (column, oldValue, row, newValue) => {
  const body = JSON.stringify(row);
  await axiosConfig.put("/api/categories", body).then(res => {
    if (res.status === 200) {
      toast("Category has been changed", {
        autoClose: 6000,
        position: "top-right"
      });
    }
  });
};

export default function AllCategories() {
  const [categories, setCategories] = useState([]);
  const [selectedRow, setSelectedRow] = useState({});

  const CaptionElement = () => (
    <React.Fragment>
      <h3
        style={{
          borderRadius: "0.25em",
          textAlign: "center",
          color: "purple",
          border: "1px solid purple",
          padding: "0.5em"
        }}
      >
        Categories
      </h3>
      <Button type="button" onClick={handleClick}>
        delete
      </Button>
    </React.Fragment>
  );

  async function handleClick() {
    await axiosConfig
      .delete("/api/categories" + "/" + selectedRow.id)
      .then(res => {
        if (res.status === 200) {
          toast("Category has been deleted", {
            autoClose: 6000,
            position: "top-right"
          });
        }
      });
  }

  const selectRow = {
    mode: "radio",
    bgColor: "#f8869b",
    clickToSelect: true,
    clickToEdit: true,
    blurToSaveCell: true,
    onSelect: (row, isSelect, rowIndex, e) => {
      setSelectedRow(row);
      console.log(selectedRow, "row selected");
    }
  };

  useEffect(() => {
    async function getCategories() {
      await axiosConfig.get("api/categories").then(res => {
        if (res.status === 200) {
          window.localStorage.setItem(
            "categories",
            res.data.map(a => a.name)
          );
          setCategories(() => res.data);
        }
      });
    }
    getCategories();
  }, []);

  return (
    <Fragment>
      <BootstrapTable
        className="app-main"
        defaultSorted={defaultSorted}
        keyField="id"
        bordered={false}
        data={categories}
        columns={columns}
        caption={<CaptionElement />}
        selectRow={selectRow}
        cellEdit={cellEditFactory({
          mode: "dbclick",
          afterSaveCell,
          blurToSaveCell: true
        })}
      />
    </Fragment>
  );
}

// <Switch>
//  <Route path="/:id" children={MealsByCategory name= />
// </Switch>
