import styled from "styled-components";

export const MealCard = styled.div`
  width: 500px;
  height: 250px;

  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-start;
  align-items: center;
  margin: auto;
  margin-bottom: 40px;
  font-size: 16px;
  border-radius: 8px;
`;
