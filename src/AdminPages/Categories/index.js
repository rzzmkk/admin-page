import React, { Fragment } from "react";
import { Route, Switch } from "react-router-dom";
import { ProtectedRoute } from "../Routes/ProtectedRoute";

import AppHeader from "../../Layout/AppHeader/";
import AppSidebar from "../../Layout/AppSidebar/";

import AllCategories from "./AllCategories";
import AddCategory from "./AddCategory";
import MealsByCategory from "./MealsByCategory";

const Meals = ({ match }) => (
  <Fragment>
    <AppHeader />
    <div className="app-main">
      <AppSidebar />
      <div className="app-main__outer">
        <div className="app-main__inner">
          <Switch>
            <ProtectedRoute
              path={`${match.url}/all`}
              component={AllCategories}
            />
            <ProtectedRoute path={`${match.url}/add`} component={AddCategory} />
            <ProtectedRoute path={"/:id"} component={MealsByCategory} />
          </Switch>
        </div>
      </div>
    </div>
  </Fragment>
);

export default Meals;
