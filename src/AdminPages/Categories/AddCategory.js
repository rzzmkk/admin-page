import React, { useState } from "react";
import styled from "styled-components";
import { Formik } from "formik";

import { axiosConfig } from "../../utils/axiosConfig";

const Button = styled.button`
  background-color: #fa788e;
  height: 50px;
  color: white;
  cursor: pointer;

  &:disabled {
    cursor: not-allowed;
  }
`;

const Message = () => <p>Category was added</p>;

export default function AddCategory() {
  const [show, setShow] = useState(false);
  return (
    <div>
      <Formik
        initialValues={{
          name: "",
          photoPath: ""
        }}
        onSubmit={async (values, { setSubmitting }) => {
          await axiosConfig.post("/categories", values).then(res => {
            if (res.status === 201) {
              setShow(() => true);
            }
          });
          setSubmitting(false);
        }}
      >
        {({ values, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
          <form className="meal-form" onSubmit={handleSubmit}>
            <div className="form-group">
              <label>Category name</label>
              <input
                type="text"
                name="name"
                onChange={handleChange}
                onBlur={handleBlur}
                value={values.name}
              />
            </div>
            {show ? <Message /> : null}
            <Button type="submit" disabled={isSubmitting && !values.name}>
              Add Category
            </Button>
          </form>
        )}
      </Formik>
    </div>
  );
}
