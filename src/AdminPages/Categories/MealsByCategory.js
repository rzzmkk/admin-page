import React, { Fragment, Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import cellEditFactory from "react-bootstrap-table2-editor";
import { axiosConfig } from "../../utils/axiosConfig";
import "./table.css";

class MealsByCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mealData: []
    };
  }

  async componentDidMount() {
    const { id } = this.props.location.props;
    await axiosConfig.get("/api/meals/category/" + id).then(res => {
      if (res.status === 200) {
        this.setState({
          mealData: res.data
        });
      }
    });
  }
  render() {
    const selectRow = {
      mode: "checkbox",
      bgColor: "#00BFFF",
      clickToSelect: true,
      clickToEdit: true
    };
    const { mealData } = this.state;
    const columns = [
      {
        dataField: "id",
        text: "ID",
        sort: true
      },
      {
        dataField: "title",
        text: "Name"
      },
      {
        dataField: "price",
        text: "Price",
        sort: true
      },
      {
        dataField: "description",
        text: "Description"
      },
      {
        dataField: "category.name",
        text: "Category"
      }
    ];
    const defaultSorted = [
      {
        dataField: "id", // if dataField is not match to any column you defined, it will be ignored.
        order: "asc" // desc or asc
      }
    ];
    const afterSaveCell = async (column, oldValue, row, newValue) => {
      row.column = newValue;
      const body = JSON.stringify(row);
      await axiosConfig.put("/api/meals", body).then(res => {
        if (res.status === 200) {
          console.log("Meal updated");
        }
      });
    };

    const CaptionElement = () => (
      <h3
        style={{
          borderRadius: "0.25em",
          textAlign: "center",
          color: "purple",
          border: "1px solid purple",
          padding: "0.5em"
        }}
      >
        {this.props.location.props.name}
      </h3>
    );

    return (
      <Fragment>
        <BootstrapTable
          defaultSorted={defaultSorted}
          keyField="id"
          bordered={false}
          data={mealData}
          columns={columns}
          caption={<CaptionElement />}
          selectRow={selectRow}
          cellEdit={cellEditFactory({
            mode: "dbclick",
            afterSaveCell,
            blurToSaveCell: true
          })}
          exportCSV
        />
      </Fragment>
    );
  }
}

export default MealsByCategory;
