import { Route, Redirect } from "react-router-dom";
import React, { Suspense, lazy, Fragment } from "react";
import Categories from "../../AdminPages/Categories";
import { ToastContainer } from "react-toastify";
import Login from "../../AdminPages/Login";
import { ProtectedRoute } from "../../AdminPages/Routes/ProtectedRoute";
import Orders from "../../AdminPages/Orders";

const Dashboards = lazy(() => import("../../DemoPages/Dashboards"));

const Components = lazy(() => import("../../DemoPages/Components"));
const Meals = lazy(() => import("../../AdminPages/Meals"));
const Users = lazy(() => import("../../AdminPages/Users/"));
// const Categories = lazy(() => import("../../AdminPages/Categories"));

const AppMain = () => {
  return (
    <Fragment>
      {/* Components */}
      <Route exact path="/" component={Login} />
      <ProtectedRoute path="/orders" component={Orders} />
      <Route path="/categories" component={Categories} />
      <Suspense
        fallback={
          <div className="loader-container">
            <div className="loader-container-inner">
              <h6 className="mt-5">
                Please wait while we load all the Meals
                <small>
                  Because this is a demonstration we load at once all the Meals.
                  This wouldn't happen in a real live app!
                </small>
              </h6>
            </div>
          </div>
        }
      >
        <Route path="/users" component={Users} />
      </Suspense>
      <Suspense
        fallback={
          <div className="loader-container">
            <div className="loader-container-inner">
              <h6 className="mt-5">
                Please wait while we load all the Meals
                <small>
                  Because this is a demonstration we load at once all the Meals.
                  This wouldn't happen in a real live app!
                </small>
              </h6>
            </div>
          </div>
        }
      >
        <Route path="/meals" component={Meals} />
      </Suspense>
      <Suspense
        fallback={
          <div className="loader-container">
            <div className="loader-container-inner">
              <h6 className="mt-5">
                Please wait while we load all the Components examples
                <small>
                  Because this is a demonstration we load at once all the
                  Components examples. This wouldn't happen in a real live app!
                </small>
              </h6>
            </div>
          </div>
        }
      >
        <Route path="/components" component={Components} />
      </Suspense>

      <Suspense
        fallback={
          <div className="loader-container">
            <div className="loader-container-inner">
              <h6 className="mt-3">
                Please wait while we load all the Dashboards examples
                <small>
                  Because this is a demonstration, we load at once all the
                  Dashboards examples. This wouldn't happen in a real live app!
                </small>
              </h6>
            </div>
          </div>
        }
      >
        <Route path="/dashboards" component={Dashboards} />
      </Suspense>

      <ToastContainer />
    </Fragment>
  );
};

export default AppMain;
