export const MainNav = [
  {
    icon: "pe-7s-rocket",
    label: "Dashboard",
    to: "/dashboards/basic"
  }
];

export const FilesNav = [
  {
    icon: "pe-7s-light",
    label: "Download file",
    to: "#"
  },
  {
    icon: "pe-7s-light",
    label: "Upload file",
    to: "#"
  },
  {
    icon: "pe-7s-light",
    label: "Download all files",
    to: "#"
  },
  {
    icon: "pe-7s-light",
    label: "Get default file",
    to: "#"
  }
];

export const MealsNav = [
  {
    icon: "pe-7s-male",
    label: "Get all meals",
    to: "/meals/all"
  },
  {
    icon: "pe-7s-female",
    label: "Add",
    to: "/meals/add"
  },
  {
    icon: "pe-7s-male",
    label: "Update meal",
    to: "/meals/update"
  },
  {
    icon: "pe-7s-male",
    label: "Delete meal",
    to: "meals/all"
  }
];

export const CategoriesNav = [
  {
    icon: "pe-7s-male",
    label: "Get all categories",
    to: "/categories/all"
  },
  {
    icon: "pe-7s-male",
    label: "Add category",
    to: "/categories/add"
  }
];

export const OrderNav = [
  {
    icon: "pe-7s-male",
    label: "Get all orders",
    to: "#"
  },
  {
    icon: "pe-7s-male",
    label: "Live orders page",
    to: "/orders/live"
  },
  {
    icon: "pe-7s-male",
    label: "Update order",
    to: "#"
  },
  {
    icon: "pe-7s-male",
    label: "Delete order",
    to: "#"
  },
  {
    icon: "pe-7s-male",
    label: "Update order",
    to: "#"
  },
  {
    icon: "pe-7s-male",
    label: "Get order by id",
    to: "#"
  },
  {
    icon: "pe-7s-male",
    label: "Delete order by id",
    to: "#"
  }
];

export const UserNav = [
  {
    icon: "pe-7s-male",
    label: "Get all users",
    to: "/users/all"
  },
  {
    icon: "pe-7s-male",
    label: "Add user",
    to: "/users/add"
  },
  {
    icon: "pe-7s-male",
    label: "Delete user",
    to: "#"
  }
];
